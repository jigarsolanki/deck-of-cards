# README #

Deck of Cards

**Notes**

* No Pocker rules were implemented - No pot or community cards dealing
* Deck class has both the must have methods
* Some of the classes have some more description about the intent of the class
* Using docker for ease in setup and deployment

## Requirements ##

* Apache Maven 3.3.9
* Java

or

* Docker

## How to setup ##

**DOCKER**

If you don't want to install any dependencies on your host machine and have docker installed just run

`bash ./scripts/docker`

Above should get you in to the docker container and you can run below commands.

note: Running docker will take sometime for the first time as the container will have to update the image and install project dependencies.


**Without Docker**

Make sure you have Java and Maven dependencies are installed as per the Requirements.

## Commands ##

* To build all the modules : `mvn clean install`
* To run unit tests : `mvn test`
* To run sample app : `mvn exec:java -Dexec.mainClass="deck.App"`