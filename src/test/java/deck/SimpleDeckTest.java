package deck;

import junit.framework.*;
import java.util.*;

public class SimpleDeckTest extends TestCase {
  Deck deck;

  public void setUp() {
    deck = new SimpleDeck();
  }

  public void testThatItHasAllCards() {
    for (FaceValue faceValue : FaceValue.values()) {
      assertNotNull(getCard(Suit.HEARTS, faceValue));
    }
  }

  public void testThatItHasAllClubsCards() {
    for (FaceValue faceValue : FaceValue.values()) {
      assertNotNull(getCard(Suit.CLUBS, faceValue));
    }
  }

  public void testThatItHasAllDiamondsCards() {
    for (FaceValue faceValue : FaceValue.values()) {
      assertNotNull(getCard(Suit.DIAMONDS, faceValue));
    }
  }

  public void testThatItHasAllSpadesCards() {
    for (FaceValue faceValue : FaceValue.values()) {
      assertNotNull(getCard(Suit.SPADES, faceValue));
    }
  }

  private Card getCard(Suit suit, FaceValue faceValue) {
    int i = 51;
    Card matchingCard = null;
    Card[] allCards = deck.getCards();

    while(matchingCard == null && i >= 0)
    {
      if (allCards[i].getFaceValue() == faceValue && allCards[i].getSuit() == suit)
      {
        matchingCard = allCards[i];
      }

      i--;
    }

    return matchingCard;
  }
}
