package deck;

import junit.framework.*;
import java.util.*;

public class CardTest extends TestCase {
  Card card;

  public void setUp() {
    card = new Card(Suit.DIAMONDS, FaceValue.ACE);
  }

  public void testThatSetsTheSuit() {
    assertEquals(Suit.DIAMONDS, card.getSuit());
  }

  public void testThatSetsTheFaceValue() {
    assertEquals(FaceValue.ACE, card.getFaceValue());
  }
}
