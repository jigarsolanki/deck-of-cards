package deck;

import junit.framework.*;
import java.util.*;

import static org.mockito.Mockito.*;

public class DealerTest extends TestCase {
  Dealer delear;
  Deck deck;
  Card card1, card2, card3, card4;
  Player player1, player2;

  public void setUp() {
    card1 = new Card(Suit.CLUBS, FaceValue.KING);
    card2 = new Card(Suit.DIAMONDS, FaceValue.QUEEN);
    card3 = new Card(Suit.HEARTS, FaceValue.TEN);
    card4 = new Card(Suit.SPADES, FaceValue.ACE);

    player1 = mock(Player.class);
    player2 = mock(Player.class);

    deck = mock(Deck.class);
    delear = new Dealer(deck, new Player[]{ player1, player2 });
  }

  public void testThatItShufflesTheDeckToBeginWith() {
    verify(deck, times(1)).shuffle();
  }

  public void testThatItAddsACardToEachPlayer() {
    when(deck.size()).thenReturn(2);

    when(deck.dealOneCard()).thenReturn(card1).thenReturn(card2);

    delear.dealACardToEachPlayer();

    verify(player1, times(1)).addCard(card1);
    verify(player2, times(1)).addCard(card2);
  }

  public void testThatItDoesNotAddACardToAnyPlayersWhenNotEnoughCards() {
    when(deck.size()).thenReturn(1);

    when(deck.dealOneCard()).thenReturn(card1).thenReturn(card2);

    delear.dealACardToEachPlayer();

    verify(player1, never()).addCard(any(Card.class));
    verify(player2, never()).addCard(any(Card.class));
  }
}
