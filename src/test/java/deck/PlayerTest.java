package deck;

import junit.framework.*;

public class PlayerTest extends TestCase {
  Player player;

  public void setUp() {
    player = new Player("Champion");
  }

  public void testThatItAddsTheCard() {
    Card card1 = new Card(Suit.CLUBS, FaceValue.KING);
    Card card2 = new Card(Suit.HEARTS, FaceValue.KING);

    player.addCard(card1);
    player.addCard(card2);

    assertEquals(player.getCards()[0], card1);
    assertEquals(player.getCards()[1], card2);
  }
}
