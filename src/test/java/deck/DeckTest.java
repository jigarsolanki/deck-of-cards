package deck;

import junit.framework.*;
import java.util.*;
import static org.mockito.Mockito.*;

public class DeckTest extends TestCase {
  Deck deck;
  Random random;
  ArrayList<Card> cards;
  Card card1, card2, card3;

  public void setUp() {
    cards = new ArrayList<Card>();

    card1 = new Card(Suit.DIAMONDS, FaceValue.ACE);
    card2 = new Card(Suit.SPADES, FaceValue.KING);
    card3 = new Card(Suit.CLUBS, FaceValue.NINE);

    cards.add(card1);
    cards.add(card2);
    cards.add(card3);

    deck = new Deck();
    deck.setCards(cards);
  }

  public void testThatItDealsTheCardsInOrder() {
    assertEquals(deck.dealOneCard(), card1);
    assertEquals(deck.dealOneCard(), card2);
    assertEquals(deck.dealOneCard(), card3);
  }

  public void testThatItDoesNotDealTheCardWhenNoMoreCardsAreAvailable() {
    deck.dealOneCard();
    deck.dealOneCard();
    deck.dealOneCard();

    assertNull(deck.dealOneCard());
  }

  public void testThatItHasRandomOrderAfterShuffling() {
    random = mock(Random.class);

    when(random.nextInt(3)).thenReturn(0);
    when(random.nextInt(2)).thenReturn(1);
    when(random.nextInt(1)).thenReturn(2);

    deck = new Deck(random);
    deck.setCards(cards);

    deck.shuffle();

    assertEquals(deck.dealOneCard(), card3);
    assertEquals(deck.dealOneCard(), card2);
    assertEquals(deck.dealOneCard(), card1);
  }
}
