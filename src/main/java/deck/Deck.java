package deck;

import java.util.*;
import java.lang.IndexOutOfBoundsException;

/**
 * This class represents the deck of cards.
 * The Deck could be 52 regular cards or only HEARTS or any other combination.
 * This class is agnostist about any combinations of cards.
 */
public class Deck {
  private ArrayList<Card> cards;
  private Random random;

  public Deck() {
    this(new Random());
  }

  public Deck(Random random) {
    this.random = random;
    this.cards = new ArrayList<Card>();
  }

  public void shuffle() {
    for (int i = (this.cards.size() - 1); i > 0; i--) {
      int index = random.nextInt(i + 1);
      Collections.swap(this.cards, i, index);
    }
  }

  public Card dealOneCard() {
    if (this.size() == 0) {
      return null;
    }

    return this.cards.remove(0);
  }

  public int size() {
    return this.cards.size();
  }

  public Card[] getCards() {
    return (Card[]) this.cards.toArray(new Card[this.cards.size()]);
  }

  public void setCards(ArrayList<Card> cards) {
    this.cards = cards;
  }
}
