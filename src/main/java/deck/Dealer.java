package deck;

/**
 * A class to represet a dealer.
 * Dealer only knows about what deck to use and who to distribute the cards to.
 * When and how many cards is not handled currently.
 */
public class Dealer {
  private Deck deck;
  private Player[] players;

  public Dealer(Deck deck, Player[] players) {
    this.deck = deck;
    this.deck.shuffle();

    this.players = players;
  }

  public void dealACardToEachPlayer() {
    if (this.deck.size() >= this.players.length) {
      for (Player player: this.players) {
        player.addCard(this.deck.dealOneCard());
      }
    }
  }
}
