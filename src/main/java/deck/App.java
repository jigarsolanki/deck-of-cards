package deck;

/**
 * Sample client code that shows how different objects interact with each other
 */
public class App {
  public static void main(String[] args) {

    Player[] players = new Player[] {
      new Player("Phil"),
      new Player("Doyle"),
      new Player("Chris")
    };

    Deck deck = new SimpleDeck();

    Dealer dealer = new Dealer(deck, players);

    dealer.dealACardToEachPlayer(); // deals the first card to each of the players
    dealer.dealACardToEachPlayer(); // deals the second card to each of the players

    showCards(players);
  }

  public static void showCards(Player[] players) {
    System.out.println("********************");
    for (Player player: players) {
      Card[] cards = player.getCards();
      String card1 = cards[0].getSuit() + "-" + cards[0].getFaceValue();
      String card2 = cards[1].getSuit() + "-" + cards[1].getFaceValue();

      System.out.println(player.getName() + ": [" + card1 + "], [" + card2 + "] ");
    }
    System.out.println("********************");
  }
}
