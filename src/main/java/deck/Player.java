package deck;

import java.util.*;

/**
 * Represents a generic player. 
 */
public class Player {
  private ArrayList<Card> cards;
  private String name;

  public Player(String name) {
    this.name = name;
    cards = new ArrayList<Card>();
  }

  public void addCard(Card card) {
    this.cards.add(card);
  }

  public Card[] getCards() {
    return (Card[]) this.cards.toArray(new Card[this.cards.size()]);
  }

  public String getName() {
    return this.name;
  }
}
