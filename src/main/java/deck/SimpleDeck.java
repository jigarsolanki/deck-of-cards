package deck;

import java.util.*;

/**
 * This class represents a regular deck with 52 card.
 */
public class SimpleDeck extends Deck {
  public SimpleDeck() {
    super();

    this.setCards(this.generateCards());
  }

  private ArrayList<Card> generateCards() {
    ArrayList<Card> availableCards = new ArrayList<Card>();

    for ( Suit suit: Suit.values() ) {
      for ( FaceValue faceValue: FaceValue.values() ) {
        availableCards.add(new Card(suit, faceValue));
      }
    }

    return availableCards;
  }
}
